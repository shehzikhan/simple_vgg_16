"""
Generic setup of the data sources and the model training. 

Based on:
    https://github.com/fchollet/keras/blob/master/examples/mnist_mlp.py
and also on 
    https://github.com/fchollet/keras/blob/master/examples/mnist_cnn.py

"""


from keras.datasets       import mnist, cifar10
from keras.models         import Model
from keras.layers         import Dense, Dropout, Flatten
from keras.utils.np_utils import to_categorical
from keras.callbacks      import EarlyStopping, Callback,ModelCheckpoint, LearningRateScheduler, TensorBoard
from keras.layers         import Conv2D, MaxPooling2D
from keras                import backend as K
from keras.preprocessing.image import ImageDataGenerator
from time import time

def get_cifar10_cnn():
    """Retrieve the MNIST dataset and process the data."""
    # Set defaults.
    nb_classes = 10 #dataset dependent 

    # the data, shuffled and split between train and test sets
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    
    # convert class vectors to binary class matrices
    y_train = to_categorical(y_train, nb_classes)
    y_test  = to_categorical(y_test,  nb_classes)

    input_shape = x_train.shape[1:]

    x_train = x_train.astype('float32')
    x_test  = x_test.astype('float32')
    x_train /= 255
    x_test  /= 255

    return (nb_classes, input_shape, x_train, x_test, y_train, y_test)

def get_mnist_cnn():
    """Retrieve the MNIST dataset and process the data."""
    # Set defaults.
    nb_classes = 10 #dataset dependent 

    
    # Input image dimensions
    img_rows, img_cols = 28, 28

    # Get the data.
    # the data, shuffled and split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    
    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)
    
    x_train = x_train.astype('float32')
    x_test  = x_test.astype('float32')
    x_train /= 255
    x_test  /= 255


    # convert class vectors to binary class matrices
    y_train = to_categorical(y_train, nb_classes)
    y_test  = to_categorical(y_test,  nb_classes)

    return (nb_classes, input_shape, x_train, x_test, y_train, y_test)



class LossHistory(Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))

def train_and_score(dataset):
    """Train the model, return test loss.

    Args:
        network (dict): the parameters of the network
        dataset (str): Dataset to use for training/evaluating

    """
    batch_size = 128
    epochs = 4

    print("Getting Keras datasets")

    if dataset == 'cifar10':
        nb_classes, input_shape, x_train, x_test, y_train, y_test = get_cifar10_cnn()
    elif dataset == 'mnist':
        nb_classes, input_shape, x_train, x_test, y_train, y_test = get_mnist_cnn()
    elif dataset == 'other':
        img_rows, img_cols, img_channels = 128, 128, 3
        nb_classes = 23
        train_data_dir = "pepper/training"
        test_data_dir = "pepper/test"
        val_data_dir = "pepper/validation"
        nb_train_samples = 4152
        nb_val_samples = 462
        nb_test_samples = 450

        train_datagen = ImageDataGenerator(
            rescale=1. / 255,
            horizontal_flip=True,
            fill_mode="nearest",
            zoom_range=0.3,
            width_shift_range=0.3,
            height_shift_range=0.3,
            rotation_range=30)

        test_datagen = ImageDataGenerator(
            rescale=1. / 255,
            horizontal_flip=True,
            fill_mode="nearest",
            zoom_range=0.3,
            width_shift_range=0.3,
            height_shift_range=0.3,
            rotation_range=30)

        val_datagen = ImageDataGenerator(
            rescale=1. / 255,
            horizontal_flip=True,
            fill_mode="nearest",
            zoom_range=0.3,
            width_shift_range=0.3,
            height_shift_range=0.3,
            rotation_range=30)

        train_generator = train_datagen.flow_from_directory(
            train_data_dir,
            target_size=(img_cols, img_rows),
            batch_size=batch_size,
            class_mode='categorical'
        )

        test_generator = test_datagen.flow_from_directory(
            test_data_dir,
            target_size=(img_cols, img_rows),
            class_mode='categorical'
        )
        val_generator = val_datagen.flow_from_directory(
            val_data_dir,
            target_size=(img_cols, img_rows),
            class_mode='categorical'
        )


    print("Compling Keras model")

    activation = "relu"
    optimizer = "sgd"


    next_input = Conv2D(64, kernel_size=(3,3), activation=activation, padding='same')(input_shape)
    next_input = Conv2D(64, kernel_size=(3,3), activation=activation, padding='same')(next_input)

    next_input = MaxPooling2D(pool_size=(2,2), padding="same", strides=(2,2))(next_input)

    next_input = Conv2D(128, kernel_size=(3,3), activation=activation, padding='same')(next_input)
    next_input = Conv2D(128, kernel_size=(3,3), activation=activation, padding='same')(next_input)

    next_input = MaxPooling2D(pool_size=(2,2), padding="same", strides=(2,2))(next_input)

    next_input = Conv2D(256, kernel_size=(3,3), activation=activation, padding='same')(next_input)
    next_input = Conv2D(256, kernel_size=(3,3), activation=activation, padding='same')(next_input)
    next_input = Conv2D(256, kernel_size=(3,3), activation=activation, padding='same')(next_input)

    next_input = MaxPooling2D(pool_size=(2,2), padding="same", strides=(2,2))(next_input)

    next_input = Conv2D(512, kernel_size=(3,3), activation=activation, padding='same')(next_input)
    next_input = Conv2D(512, kernel_size=(3,3), activation=activation, padding='same')(next_input)
    next_input = Conv2D(512, kernel_size=(3,3), activation=activation, padding='same')(next_input)

    next_input = MaxPooling2D(pool_size=(2,2), padding="same", strides=(2,2))(next_input)

    next_input = Conv2D(512, kernel_size=(3,3), activation=activation, padding='same')(next_input)
    next_input = Conv2D(512, kernel_size=(3,3), activation=activation, padding='same')(next_input)
    next_input = Conv2D(512, kernel_size=(3,3), activation=activation, padding='same')(next_input)

    next_input = MaxPooling2D(pool_size=(2,2), padding="same", strides=(2,2))(next_input)

    output = Flatten()(next_input)
    output = Dense(4096, activation=activation)(output)
    output = Dropout(0.5)(output)
    output = Dense(nb_classes, activation='softmax')(output)

    model = Model(inputs=[input_shape], outputs=[output])

    model.compile(loss='categorical_crossentropy',optimizer=optimizer,metrics=['accuracy'])


    ######################################## Defining Optional Callbacks ####################################################
    history = LossHistory()

    # In your case, you can see that your training loss is not dropping - which means you are learning nothing after each epoch.
    # It look like there's nothing to learn in this model, aside from some trivial linear-like fit or cutoff value.
    early_stopper = EarlyStopping(monitor='val_loss', min_delta=0.1, patience=2, verbose=0, mode='auto')

    # Save the model according to the conditions
    filepath_checkpoint = "chekcpoints/weights.{epoch:02d}-{val_loss:.2f}.hdf5"
    checkpoint = ModelCheckpoint(filepath_checkpoint, monitor='val_acc', verbose=1, save_best_only=True,save_weights_only=False, mode='auto', period=1)

    tensorboard = TensorBoard(
        log_dir='logs/{}'.format(time()),
        histogram_freq=0,
        write_images=True,
        write_graph=True,
        batch_size=256
    )

    ###################################### Training the Model ############################################################
    if dataset in ["cifar10","mnist"]:
        model.fit(x_train, y_train,
                  batch_size=batch_size,
                  epochs=epochs,
                  verbose=1,
                  validation_data=(x_test, y_test),
                  #callbacks=[history])
                  callbacks=[early_stopper])

        score = model.evaluate(x_test, y_test, verbose=0)

    elif dataset=="other":
        model.fit_generator(
            train_generator,
            epochs=epochs,
            steps_per_epoch=nb_train_samples / batch_size,
            validation_data=val_generator,
            # validation_split=0.2
            callbacks=[checkpoint, tensorboard]
        )

        score = model.evaluate_generator(test_generator, verbose=0)

    ###################################### Testing the model #####################################################


    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

    K.clear_session()

    
    return score[1]  # 1 is accuracy. 0 is loss.

if __name__=="__main__":
    train_and_score("mnist")